import {put, takeEvery} from 'redux-saga/effects'
import api from '../../api'
import hub from '../../hub'
function * login (action) {
  let {username, password} = action
  if (!username || !password) return null
  let req = api(hub.getAuthToken())
  let res = yield req.login({username, password})
  hub.setAuthToken(res.data.payload.token)
  yield put({type: 'pages.toMountPoint'})
}
export default function * () {
  yield takeEvery('api.login', login)
}
