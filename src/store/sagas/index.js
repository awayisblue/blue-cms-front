const context = require.context('.', true, /\.js$/)
let all = []
context.keys().forEach((key) => {
  let name = key.match(/\.\/(\w+)\.js/)[1]
  if (name && name !== 'index') {
    all.push(context(key).default())
  }
})
export default function * () {
  yield all
}
