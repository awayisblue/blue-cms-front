import {put, takeEvery} from 'redux-saga/effects'
import {delay} from 'redux-saga'
function * open (action) {
  yield delay(2000)
  yield put({type: 'notification.close'})
}
export default function * () {
  yield takeEvery('notification.open', open)
}
