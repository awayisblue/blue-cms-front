import reducers from './reducers'
import initializers from './initializers'
import sagas from './sagas'
import {createStore, applyMiddleware} from 'redux'
import createSagaMiddleware from 'redux-saga'
const sagaMiddleware = createSagaMiddleware()
let store = createStore(reducers, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(sagas)
initializers(store)
export default store
