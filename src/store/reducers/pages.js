let copyState = (state) => JSON.parse(JSON.stringify(state))
export default {
  state: ['mountPoint'],
  reducers: {
    toLogin (state, action) {
      return ['login']
    },
    toMountPoint (state, action) {
      return ['mountPoint']
    },
    toConfig (state, action) {
      return ['config']
    },
    stackLogin (state) {
      let copy = copyState(state)
      copy.push('login')
      return copy
    },
    stackMountPoint (state) {
      let copy = copyState(state)
      copy.push('mountPoint')
      return copy
    },
    stackConfig (state) {
      let copy = copyState(state)
      copy.push('config')
      return copy
    },
    back (state) {
      let copy = copyState(state)
      copy.pop()
      console.log(copy)
      return copy
    }
  }
}
