
import {combineReducers} from 'redux'
import reduxShape from 'redux-shape'
const context = require.context('.', true, /\.js$/)
let shape = {}
context.keys().forEach((key) => {
  let name = key.match(/\.\/(\w+)\.js/)[1]
  if (name && name !== 'index') {
    shape[name] = () => context(key).default
  }
})
const reducers = reduxShape(combineReducers, {
  shape: shape
})
export default reducers
