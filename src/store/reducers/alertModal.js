export default {
  state: {
    open: false,
    title: '提示',
    text: '确定吗？'
  },
  reducers: {
    open (state, action) {
      let data = {
        open: false,
        title: '提示',
        text: '确定吗？'
      }
      let {title, text} = action
      title && (data.title = title)
      text && (data.text = text)
      data.open = true
      return data
    },
    close (state, action) {
      return {
        open: false,
        title: '提示',
        text: '确定吗？'
      }
    }
  }
}
