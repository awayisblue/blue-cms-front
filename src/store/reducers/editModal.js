export default {
  state: {
    open: false,
    title: '编辑',
    fields: [],
    constrains: {}
  },
  reducers: {
    open (state, action) {
      let data = {
        open: false,
        title: '编辑',
        fields: [],
        constrains: {}
      }
      let {title, fields, constrains} = action
      title && (data.title = title)
      fields && (data.fields = fields)
      constrains && (data.constrains = constrains)
      data.open = true
      return data
    },
    close (state, action) {
      return {
        open: false,
        title: '编辑',
        fields: [],
        constrains: {}
      }
    }
  }
}
