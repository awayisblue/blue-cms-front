export default {
  state: {
    open: false,
    text: '未知提示',
    level: 'primary'
  },
  reducers: {
    open (state, action) {
      let data = {
        open: false,
        text: '未知提示',
        level: 'primary'
      }
      let {text, level} = action
      text && (data.text = text)
      level && (data.level = level)
      data.open = true
      return data
    },
    close (state, action) {
      return {
        open: false,
        text: '未知提示',
        level: 'primary'
      }
    }
  }
}
