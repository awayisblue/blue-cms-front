import axios from 'axios'
import hub from '../hub'
let env = window.ENV
let urlBase = `${env.cmsHost}/${env.cmsPrefix}/_mount-point`
export default (authToken, mountPoint) => {
  authToken = authToken || hub.getAuthToken()
  let config = {
    baseURL: urlBase,
    timeout: 2000,
    headers: {'Authorization': `Bearer ${authToken}`}
  }
  if (mountPoint) config.headers['x-mount-point'] = mountPoint
  let instance = axios.create(config)
  instance.interceptors.response.use((res) => {
    if (checkRes(res)) return Promise.resolve(res)
    let store = hub.getStore()
    store.dispatch({type: 'notification.open', text: 'api数据返回格式不符合要求', level: 'danger'})
  }, (err) => {
    let store = hub.getStore()
    if (!err.response) {
      store.dispatch({type: 'notification.open', text: '服务器无返回数据', level: 'danger'})
    } else if (!mountPoint && err.response.status === 403) {
      store.dispatch({type: 'pages.toLogin'})
    } else {
      store.dispatch({type: 'notification.open', text: err.response.data.msg, level: 'danger'})
    }
    return Promise.reject(err)
  })
  return {
    async get (url, params) {
      return instance.get(url, params)
    },
    async post (url, params) {
      return instance.post(url, params)
    },
    async put (url, params) {
      return instance.put(url, params)
    },
    async del (url, params) {
      return instance.delete(url, params)
    },
    async getMenus () {
      let url = `/menus`
      return instance.get(url)
    },
    async getMountPoints () {
      let url = `/mount-points`
      return instance.get(url)
    },
    async getMountPoint (name) {
      let url = `/mount-points/${name}`
      return instance.get(url)
    },
    async login (data) {
      let url = `${env.cmsHost}/${env.cmsPrefix}/account/login`
      return instance.post(url, data)
    }
  }
}

function checkRes (res) {
  let keyMap = {
    code: 'number',
    msg: 'string',
    payload: 'object'
  }
  let keys = Object.getOwnPropertyNames(res.data)
  for (let i = 0; i < keys.length; i++) {
    let key = keys[i]
    let keyType = typeof res.data[key]
    if (keyType !== keyMap[key]) return false
  }
  return true
}
