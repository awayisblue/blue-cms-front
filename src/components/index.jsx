import React, { Component } from 'react'
import Pages from './Pages'
import AlertModal from './AlertModal'
import EditModal from './EditModal'
import Notification from './Notification'
import Back from './Back'
class Root extends Component {
  render () {
    return (
      <div>
        <Pages />
        <AlertModal />
        <EditModal />
        <Notification />
        <Back />
      </div>
    )
  }
}
export default Root
