import React, { Component } from 'react'
import {connect} from 'react-redux'
import style from './styles.less'
/**
 * docs
 */
class Notification extends Component {
  constructor (props) {
    super(props)
    this.levelStylesMap = {
      'primary': style.primary,
      'secondary': style.secondary,
      'success': style.success,
      'danger': style.danger,
      'warning': style.warning,
      'info': style.info,
      'light': style.light,
      'dark': style.dark
    }
  }
  render () {
    let {text, level, open} = this.props.notification
    let levelStyle = this.levelStylesMap[level] || this.levelStylesMap['primary']
    if (!open) return null
    return (
      <div className={style.base}>
        <div className={[style.wrapper, levelStyle].join(' ')}>
          {text}
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    notification: state.notification
  }
}
export default connect(mapStateToProps, null)(Notification)
