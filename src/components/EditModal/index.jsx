import React, { Component } from 'react'
import Edit from '../common/Edit'
import {connect} from 'react-redux'
import style from './styles.less'
import hub from '../../hub'
/**
 * docs
 */
class EditModal extends Component {
  render () {
    let {title, fields, open, constrains} = this.props.editModal
    if (!open) return null
    return (
      <div className={style.overlay}>
        <div className={style.wrapper}>
          <Edit
            title={title}
            fields={fields}
            constrains={constrains}
            onConfirm={(data) => {
              let callback = hub.getEditConfirmCallback()
              callback && callback(data)
              this.props.closeModal()
            }}
            onCancel={() => {
              let callback = hub.getEditCancelCallback()
              callback && callback()
              this.props.closeModal()
            }}
          />
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    editModal: state.editModal
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    closeModal: () => {
      hub.setEditCancelCallback(() => {})
      hub.setEditConfirmCallback(() => {})
      dispatch({type: 'editModal.close'})
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditModal)
