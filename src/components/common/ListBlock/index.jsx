import React, { Component } from 'react'
import style from './styles.less'
import Button from '../Button'
import Title from '../Title'
/**
 * docs
 */
class ListBlock extends Component {
  constructor (props) {
    super(props)
    this.mehtodTextMap = {
      // 'get': '查',
      'post': '增',
      'put': '改',
      'delete': '删'
    }
  }
  render () {
    let {list, name, methods, onMethodClick, activeIndex, onSelect} = this.props
    let getable = ~methods.indexOf('get')
    let List = list.map((item, index) => {
      return <div onClick={getable ? () => onSelect(index) : null} key={index} className={[style.listItem, getable ? null : style.disable, activeIndex === index ? style.active : null].join(' ')}>
        {item.name}
      </div>
    })
    let dropDowns = methods.map((method, index) => this.mehtodTextMap[method] ? {name: this.mehtodTextMap[method], id: method} : null)
    return (
      <div className={style.base}>
        <Title title={name}>
          <Button dropDowns={dropDowns} onDropDownClick={(name, method) => onMethodClick && onMethodClick(method)} type={'warning'}>操作</Button>
        </Title>
        <div className={style.list}>
          {List}
        </div>
      </div>
    )
  }
}
export default ListBlock
