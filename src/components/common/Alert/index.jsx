import React, { Component } from 'react'
import style from './styles.less'
import Button from '../Button'
import Title from '../Title'
/**
 * docs
 */
class Alert extends Component {
  render () {
    let {title = '提示', text = '是否确定？', onCancel, onConfirm} = this.props
    return (
      <div className={style.base}>
        <Title title={title} />
        <div className={style.confirm}>
          {text}
        </div>
        <div className={style.buttons}>
          <Button type={'secondary'} onClick={onCancel}>取消</Button>
          <Button type={'success'} onClick={onConfirm}>确定</Button>
        </div>
      </div>

    )
  }
}
export default Alert
