import React, { Component } from 'react'
import style from './styles.less'

/**
 * docs
 */
class Title extends Component {
  render () {
    let {title = '标题', children} = this.props
    return (
      <div className={style.title}>
        <div className={style.wrapper}>
          <div className={style.text}>{title}</div>
          <div className={style.buttons}>
            {children}
          </div>
        </div>
      </div>
    )
  }
}
export default Title
