import React, { Component } from 'react'
import style from './styles.less'

/**
 * docs
 */
class Button extends Component {
  constructor (props) {
    super(props)
    this.state = {
      dropDown: false
    }
    this.toggleDropDown = this.toggleDropDown.bind(this)
    this.typeStylesMap = {
      'primary': style.primary,
      'secondary': style.secondary,
      'success': style.success,
      'danger': style.danger,
      'warning': style.warning,
      'info': style.info,
      'light': style.light,
      'dark': style.dark
    }
  }
  toggleDropDown () {
    this.setState({
      dropDown: !this.state.dropDown
    })
  }
  render () {
    let {dropDowns = [], onDropDownClick, onClick, type, children} = this.props
    let buttonStyle = this.typeStylesMap[type] || this.typeStylesMap['primary']
    let DropDowns = dropDowns.map((item, index) => {
      return item ? <div key={index} onClick={() => {
        onDropDownClick && onDropDownClick(item.name, item.id)
        this.toggleDropDown()
      }}>
        {item.name}
      </div> : null
    })
    return (
      <div className={[style.button, buttonStyle].join(' ')} onClick={dropDowns.length > 0 ? this.toggleDropDown : onClick}>
        <div>{children}</div>
        {this.state.dropDown ? <div className={style.dropDown} onClick={(e) => e.stopPropagation()}>
          {DropDowns}
        </div> : null}
      </div>
    )
  }
}
export default Button
