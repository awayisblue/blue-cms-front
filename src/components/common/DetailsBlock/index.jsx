import React, { Component } from 'react'
import style from './styles.less'
import Button from '../Button'
import Title from '../Title'
/**
 * docs
 */
class DetailsBlock extends Component {
  constructor (props) {
    super(props)
    this.mehtodTextMap = {
      'put': '改',
      'delete': '删'
    }
    this.state = {
      dropDown: false
    }
    this.toggleDropDown = this.toggleDropDown.bind(this)
  }
  toggleDropDown () {
    this.setState({
      dropDown: !this.state.dropDown
    })
  }
  render () {
    let {list, name, methods, onMethodClick, allowConfig, onConfig} = this.props
    let List = list.map((item, index) => {
      return <div key={index} className={[style.listItem].join(' ')}>
        <div className={style.name}>
          {item.name}
        </div>
        <div className={style.value}>
          {item.value}
        </div>
      </div>
    })
    let dropDowns = methods.map((method, index) => this.mehtodTextMap[method] ? {name: this.mehtodTextMap[method], id: method} : null)
    return (
      <div className={style.base}>
        <Title title={name}>
          {allowConfig ? <Button onClick={onConfig}>配置</Button> : null}
          <Button dropDowns={dropDowns} onDropDownClick={(name, method) => onMethodClick && onMethodClick(method)} type={'warning'}>操作</Button>
        </Title>
        <div className={style.list}>
          {List}
        </div>
      </div>
    )
  }
}
export default DetailsBlock
