import React, { Component } from 'react'
import validate from 'validate.js'
import style from './styles.less'
import Button from '../Button'
import Title from '../Title'
/**
 * docs
 */
class Edit extends Component {
  constructor (props) {
    super(props)
    let {title = '编辑', fields = [], constrains = {}, onConfirm, onCancel} = this.props
    this.title = title
    this.fields = fields
    this.state = this.genState(fields)
    this.onConfirm = onConfirm
    this.onCancel = onCancel
    this.constrsins = constrains
    this.updateValue = this.updateValue.bind(this)
    this.validateConfirm = this.validateConfirm.bind(this)
  }
  genState (fields) {
    let initState = {}
    fields.forEach((field) => {
      let {key, value} = field
      key && (initState[key] = {value} || {value: ''})
    })
    return initState
  }
  updateValue (key, value) {
    let constrain = this.constrsins[key] ? {[key]: this.constrsins[key]} : null
    let errorMsg = ''
    if (constrain) {
      let data = {[key]: value}
      let error = validate(data, constrain)
      error && error[key] && (errorMsg = error[key][0])
    }
    this.setState({[key]: {value, error: errorMsg}})
    // 返回是否出错
    return !!errorMsg
  }
  validateConfirm () {
    let kvs = {}
    let error = false
    Object.getOwnPropertyNames(this.state).forEach((key) => {
      let value = this.state[key].value
      kvs[key] = value
      if (this.updateValue(key, value)) {
        error = true
      }
    })
    return error ? false : kvs
  }
  render () {
    let Fields = this.fields.map((field, index) => {
      let {name, key, desc} = field
      return <div key={index} className={style.inputWrapper}>
        <div className={[style.error, this.state[key].error ? null : style.hide].join(' ')}>
          {this.state[key].error}
        </div>
        <label>{name || key} <p className={style.tooltip}>{desc || '字段没有说明'}</p></label>
        <input type='text' value={this.state[key].value} onChange={(event) => this.updateValue(key, event.target.value)} />
      </div>
    })
    return (
      <div className={style.base}>
        <Title title={this.title} />
        <div className={style.fields}>
          {Fields}
        </div>
        <div className={style.buttons}>
          <Button type={'secondary'} onClick={this.onCancel}>取消</Button>
          <Button type={'success'} onClick={() => {
            let data = this.validateConfirm()
            data && this.onConfirm(data)
          }}>确定</Button>
        </div>
      </div>
    )
  }
}
export default Edit
