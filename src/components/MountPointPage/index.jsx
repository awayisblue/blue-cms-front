import BaseListDetailsPage from '../BaseMenusDetailsPage'
import {connect} from 'react-redux'
import hub from '../../hub'
class MountPointPage extends BaseListDetailsPage {
  constructor (props) {
    super(props)
    this.title = 'cms主页'
  }
  getAllowConfigAndMountPoint (detailsFieldList) {
    let allowConfigField = detailsFieldList.find((field) => field.key === 'allowCMS')
    let allowConfig = allowConfigField && allowConfigField.value === 'Y'
    let mountPointField = detailsFieldList.find((field) => field.key === 'mountPoint')
    let mountPoint = mountPointField && mountPointField.value
    return {
      allowConfig,
      mountPoint
    }
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    alert: ({title, text, onConfirm, onCancel}) => {
      dispatch({type: 'alertModal.open', title, text})
      // 函数不能放store，所以在hub里进行登记，alertModal会在hub拿callback来调用
      hub.setAlertConfirmCallback(onConfirm || (() => {}))
      hub.setAlertCancelCallback(onCancel || (() => {}))
    },
    edit: ({title, fields, constrains, onConfirm, onCancel}) => {
      dispatch({type: 'editModal.open', title, fields, constrains})
      // 函数不能放store，所以在hub里进行登记，alertModal会在hub拿callback来调用
      hub.setEditConfirmCallback(onConfirm || (() => { console.log('i am ') }))
      hub.setEditCancelCallback(onCancel || (() => {}))
    },
    onConfig: (mountPoint) => {
      hub.setConfigMountPoint(mountPoint)
      dispatch({type: 'pages.stackConfig'})
    }
  }
}
export default connect(null, mapDispatchToProps)(MountPointPage)
