import React, { Component } from 'react'
import {connect} from 'react-redux'
import style from './styles.less'
class Back extends Component {
  render () {
    let pages = this.props.pages
    let shouldShow = pages.length > 1
    if (!shouldShow) return null
    return (
      <div className={style.base} onClick={this.props.back}>
        返回
      </div>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    pages: state.pages
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    back () {
      dispatch({type: 'pages.back'})
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Back)
