import React, { Component } from 'react'
import ListBlock from '../common/ListBlock'
import DetailsBlock from '../common/DetailsBlock'
import style from './styles.less'
import api from '../../api'
class BaseMenusDetailsPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      menus: [],
      menusIndex: -1,
      listItemIndex: -1,
      detailsFieldList: []
    }
    this.title = '主页'
    this.mountPoint = null
    this.onSelect = this.onSelect.bind(this)
    this.fetchListOfMenu = this.fetchListOfMenu.bind(this)
    this.fetchListItem = this.fetchListItem.bind(this)
    this.getPath = this.getPath.bind(this)
    this.onMenuMethodClick = this.onMenuMethodClick.bind(this)
    this.onDetailsMethodClick = this.onDetailsMethodClick.bind(this)
    this.extractMenuFields = this.extractMenuFields.bind(this)
    this.getAllowConfigAndMountPoint = this.getAllowConfigAndMountPoint.bind(this)
  }
  static getListOfMenusKey (menusIndex) {
    return `listOfMenus${menusIndex}`
  }
  componentWillMount () {
    let req = api(null, this.mountPoint)
    req.getMenus().then((res) => {
      let menus = res.data.payload.list
      this.setState({menus}, () => {
        for (let i = 0; i < menus.length; i++) {
          this.fetchListOfMenu(i)
        }
      })
    }).catch((err) => console.log(err))
  }
  extractMenuFields (menusIndex, method = 'post') {
    let menu = this.state.menus[menusIndex]
    let fieldInfos = {}
    let constrains = JSON.parse(JSON.stringify(menu.fields))
    // console.log(JSON.stringify(constrains))
    Object.getOwnPropertyNames(constrains).forEach((key) => {
      fieldInfos[key] = constrains[key].fieldInfo || {}
      delete constrains[key].fieldInfo
      if (fieldInfos[key].noEdit && method === 'put') delete constrains[key]
    })
    let fields = []
    Object.getOwnPropertyNames(constrains).forEach((field) => {
      let fieldInfo = fieldInfos[field]
      fields.push({
        name: fieldInfo.name || field,
        key: field,
        value: '',
        desc: fieldInfo.desc
      })
    })
    return {
      constrains,
      fieldInfos,
      fields
    }
  }
  getPath (menusIndex, listItemIndex) {
    let menu = this.state.menus[menusIndex]
    if (listItemIndex === undefined) return menu.path
    let listOfMenusKey = BaseMenusDetailsPage.getListOfMenusKey(menusIndex)
    let listOfMenu = this.state[listOfMenusKey]
    if (listOfMenu) return listOfMenu[listItemIndex].path
    return null
  }
  fetchListOfMenu (menusIndex) {
    let req = api(null, this.mountPoint)
    let listOfMenusKey = BaseMenusDetailsPage.getListOfMenusKey(menusIndex)
    req.get(this.getPath(menusIndex)).then((res) => this.setState({
      [listOfMenusKey]: res.data.payload.list,
      listItemIndex: -1
    }))
  }
  fetchListItem (menusIndex, listItemIndex) {
    let req = api(null, this.mountPoint)
    let {fields} = this.extractMenuFields(menusIndex)
    req.get(this.getPath(menusIndex, listItemIndex)).then((res) => {
      let data = res.data.payload.data
      fields.forEach((field, index) => {
        if (data[field.key]) {
          fields[index].value = data[field.key]
        }
      })
      this.setState({
        detailsFieldList: fields
      })
    })
  }
  onSelect (menusIndex, listItemIndex) {
    this.fetchListItem(menusIndex, listItemIndex)
    this.setState({menusIndex, listItemIndex})
  }
  onMenuMethodClick (menusIndex, method) {
    let path = this.getPath(menusIndex)
    switch (method) {
      case 'post':
        let {constrains, fields} = this.extractMenuFields(menusIndex, 'post')
        this.props.edit({
          title: '新建',
          fields: fields,
          constrains: constrains,
          onConfirm: (data) => {
            api(null, this.mountPoint).post(path, data).then(() => this.fetchListOfMenu(menusIndex))
          }
        })
        break
      case 'delete':
        this.props.alert({
          title: '提示',
          text: '是否确定删除？',
          onConfirm: () => {
            api(null, this.mountPoint).del(path).then(() => this.fetchListOfMenu(menusIndex))
          }
        })
        break
    }
  }
  onDetailsMethodClick (menusIndex, listItemIndex, method) {
    let path = this.getPath(menusIndex, listItemIndex)
    switch (method) {
      case 'put':
        let {constrains, fields} = this.extractMenuFields(menusIndex, 'put')
        let curFields = this.state.detailsFieldList
        fields.forEach((field, index) => {
          let replace = curFields.find((curField) => field.key === curField.key)
          if (replace) fields[index] = replace
        })
        this.props.edit({
          title: '修改',
          fields: fields,
          constrains: constrains,
          onConfirm: (data) => {
            api(null, this.mountPoint).put(path, data).then(() => this.fetchListItem(menusIndex, listItemIndex))
          }
        })
        break
      case 'delete':
        this.props.alert({
          title: '提示',
          text: '是否确定删除？',
          onConfirm: () => {
            api(null, this.mountPoint).del(path).then(() => this.fetchListOfMenu(menusIndex))
          }
        })
        break
    }
  }
  getAllowConfigAndMountPoint (detailsFieldList) {
    return {
      allowConfig: false,
      mountPoint: ''
    }
  }
  render () {
    let Menus = this.state.menus.map((item, index) => {
      return <ListBlock
        key={index}
        methods={item.methods || []}
        onMethodClick={(method) => this.onMenuMethodClick(index, method)}
        list={this.state[BaseMenusDetailsPage.getListOfMenusKey(index)] || []}
        name={item.name}
        activeIndex={this.state.menusIndex === index ? this.state.listItemIndex : -1}
        onSelect={(listItemIndex) => this.onSelect(index, listItemIndex)}
      />
    })
    let {menusIndex, listItemIndex, detailsFieldList = []} = this.state
    let shouldShowDetails = menusIndex > -1 && listItemIndex > -1
    let {allowConfig, mountPoint} = this.getAllowConfigAndMountPoint(detailsFieldList)

    return (
      <div className={style.base}>
        <div className={style.title}>{this.title}</div>
        <div className={style.wrapper}>
          {Menus}
          {shouldShowDetails ? <DetailsBlock
            methods={this.state[BaseMenusDetailsPage.getListOfMenusKey(this.state.menusIndex)][this.state.listItemIndex]['methods'] || []}
            onMethodClick={(method) => this.onDetailsMethodClick(menusIndex, listItemIndex, method)}
            list={detailsFieldList}
            name={'详情'}
            allowConfig={allowConfig}
            onConfig={() => this.props.onConfig(mountPoint)}
          /> : null}
        </div>
      </div>
    )
  }
}
export default BaseMenusDetailsPage
