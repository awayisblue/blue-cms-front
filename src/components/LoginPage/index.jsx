import React, { Component } from 'react'
import Edit from '../common/Edit'
import {connect} from 'react-redux'
import style from './styles.less'
/**
 * docs
 */
class LoginPage extends Component {
  render () {
    return (
      <div className={style.overlay}>
        <div className={style.wrapper}>
          <Edit
            title={'请登录'}
            fields={[{key: 'username', name: '用户名', value: ''}, {key: 'password', name: '密码', value: ''}]}
            onConfirm={(fields) => this.props.login(fields)}
          />
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    login: ({username, password}) => {
      dispatch({type: 'api.login', username, password})
    }
  }
}
export default connect(null, mapDispatchToProps)(LoginPage)
