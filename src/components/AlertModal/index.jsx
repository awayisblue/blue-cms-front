import React, { Component } from 'react'
import Alert from '../common/Alert'
import {connect} from 'react-redux'
import style from './styles.less'
import hub from '../../hub'
/**
 * docs
 */
class AlertModal extends Component {
  render () {
    let alertModal = this.props.alertModal
    if (!alertModal.open) return null
    return (
      <div className={style.overlay}>
        <div className={style.wrapper}>
          <Alert
            title={alertModal.title}
            text={alertModal.text}
            onConfirm={(fields) => {
              let callback = hub.getAlertConfirmCallback()
              callback && callback(fields)
              this.props.closeModal()
            }}
            onCancel={() => {
              let callback = hub.getAlertCancelCallback()
              callback && callback()
              this.props.closeModal()
            }}
          />
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    alertModal: state.alertModal
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    closeModal: () => {
      hub.setAlertCancelCallback(() => {})
      hub.setAlertConfirmCallback(() => {})
      dispatch({type: 'alertModal.close'})
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AlertModal)
