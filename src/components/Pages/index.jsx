import React, { Component } from 'react'
import {connect} from 'react-redux'
import LoginPage from '../LoginPage'
import MountPointPage from '../MountPointPage'
import ConfigPage from '../ConfigPage'
import style from './styles.less'
class Pages extends Component {
  constructor (props) {
    super(props)
    this.currentPageMap = {
      'login': LoginPage,
      'mountPoint': MountPointPage,
      'config': ConfigPage
    }
  }
  render () {
    let pages = this.props.pages
    let curPage = pages[pages.length - 1]
    let Page = this.currentPageMap[curPage]
    if (!Page) return null
    return (
      <div className={style.base}>
        <Page />
      </div>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    pages: state.pages
  }
}
export default connect(mapStateToProps, null)(Pages)
