let hub = {}
export default {
  setAuthToken (authToken) {
    hub['authToken'] = authToken
    window.localStorage.setItem('authToken', authToken)
  },
  getAuthToken () {
    return hub['authToken'] || window.localStorage.getItem('authToken')
  },
  setStore (store) {
    hub['store'] = store
  },
  getStore () {
    return hub['store']
  },
  setConfigMountPoint (mountPoint) {
    hub['configMountPoint'] = mountPoint
  },
  getConfigMountPoint () {
    return hub['configMountPoint']
  },
  setAlertConfirmCallback (callback) {
    hub['alertConfirmCallback'] = callback
  },
  getAlertConfirmCallback () {
    return hub['alertConfirmCallback']
  },
  setAlertCancelCallback (callback) {
    hub['alertCancelCallback'] = callback
  },
  getAlertCancelCallback () {
    return hub['alertCancelCallback']
  },
  setEditCancelCallback (callback) {
    hub['editCancelCallback'] = callback
  },
  getEditCancelCallback () {
    return hub['editCancelCallback']
  },
  setEditConfirmCallback (callback) {
    hub['editConfirmCallback'] = callback
  },
  getEditConfirmCallback () {
    return hub['editConfirmCallback']
  }
}
