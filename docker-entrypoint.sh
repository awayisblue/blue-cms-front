#!/bin/bash
set -eo pipefail

declare -A ENV_PARAMS=(
  # cms
  ["CMS_HOST"]='http://localhost:3300'
  ["CMS_PREFIX"]="cms"

)
for index in "${!ENV_PARAMS[@]}";
do
  ENV_VAL=`eval echo '$'${index}`
  [ -z "${ENV_VAL}" ] && ENV_VAL=${ENV_PARAMS[$index]}
  /bin/sed -i "s!${index}!${ENV_VAL}!g" /usr/share/nginx/html/*.html;
done

exec "$@"
